
#pragma warning(disable : 4018 4307 4996)

#include "GSCreate.cpp"
#include "GSDrawTest.cpp"
#include "GSLoading.cpp"
#include "GSMenu.cpp"
#include "GSRunning.cpp"
#include "GSTransition.cpp"

#if defined(USE_SW_RENDER)
	#include "GSDrawTestSW.cpp"
#endif