#ifndef SYS_TIME_H
#define SYS_TIME_H

namespace sys {
    unsigned int GetTicks();
    extern unsigned int cached_time;
}

#endif // SYS_TIME_H
