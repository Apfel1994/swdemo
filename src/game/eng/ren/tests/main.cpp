
void test_anim();
void test_material();
void test_mesh();
void test_program();
void test_render_state();
void test_sparse_array();

int main() {
    test_anim();
    test_material();
    test_mesh();
    test_program();
    test_render_state();
    test_sparse_array();
}