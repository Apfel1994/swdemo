#ifndef ENG_CONFIG_H
#define ENG_CONFIG_H

const int UPDATE_RATE   = 60;
const int UPDATE_DELTA  = 1000 / UPDATE_RATE;

const char STATE_MANAGER_KEY[]  = "state_manager";
const char INPUT_MANAGER_KEY[]  = "input_manager";

const char UI_RENDERER_KEY[]    = "ui_renderer";
const char UI_ROOT_KEY[]        = "ui_root";

#endif // ENG_CONFIG_H